#!/bin/bash

VM_NAME=dev

echo "======================================================================================================="
echo "=== build jar file"
echo "======================================================================================================="

mvn clean package -PbuildDockerWorkDir -DskipTests=true

echo "======================================================================================================="
echo "=== build docker image"
echo "======================================================================================================="

docker build \
  -t mapp/demo-helloworld-web04:latest \
	-f $(pwd)/target/workdir-docker/Dockerfile \
	$(pwd)/target/workdir-docker

echo "======================================================================================================"
echo "=== run newly created jar file in docker-compose environment without creating docker image before"
echo "======================================================================================================"

docker-compose -f ./src/main/docker/s04-docker-compose.yaml -p dmo up -d

echo "======================================================================================================"
echo "=== let us wait for Tomcat ..."
echo "======================================================================================================"

docker-compose -f ./src/main/docker/s04-docker-compose.yaml -p dmo logs app

echo "======================================================================================================"
echo "=== let us test a little bit"
echo "======================================================================================================"

http -v http://$(docker-machine ip $VM_NAME):8080

echo "============================================================================================="
echo ""

http -v http://$(docker-machine ip $VM_NAME):8080/todos

echo "======================================================================================================"
echo "=== let us stop all container from this composition"
echo "======================================================================================================"

docker-compose -f ./src/main/docker/s04-docker-compose.yaml -p dmo stop
