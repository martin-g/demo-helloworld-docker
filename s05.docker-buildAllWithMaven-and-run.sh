#!/bin/bash

VM_NAME=dev

echo "======================================================================================================"
echo "=== build jar file and docker image"
echo "======================================================================================================"

mvn clean package -PbuildDockerImageWithMaven -DskipTests=true

echo "======================================================================================================"
echo "=== run newly created jar file in docker-compose environment without creating docker image before"
echo "======================================================================================================"

docker-compose -f ./src/main/docker/s05-docker-compose.yaml -p dmo up -d

echo "======================================================================================================"
echo "=== let us wait for Tomcat ..."
echo "======================================================================================================"

docker-compose -f ./src/main/docker/s05-docker-compose.yaml -p dmo logs app

echo "======================================================================================================"
echo "=== now let us test a little bit"
echo "======================================================================================================"

http -v http://$(docker-machine ip $VM_NAME):8080

echo "============================================================================================="
echo ""

http -v http://$(docker-machine ip $VM_NAME):8080/todos

echo "======================================================================================================"
echo "=== let us stop all container from this composition"
echo "======================================================================================================"

docker-compose -f ./src/main/docker/s05-docker-compose.yaml -p dmo stop
